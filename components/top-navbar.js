import { template, TemplateClass } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './top-navbar.html';

@template('InspiniaTopNavbar')
class InspiniaTopNavbar extends TemplateClass {
  props = new SimpleSchema({
    logout: Function,
  });

  $body = '';

  $side = '';

  onRendered() {
    // FIXED TOP NAVBAR OPTION
    // Uncomment this if you want to have fixed top navbar
    // this.$body.addClass('fixed-nav');
    // $(".navbar-static-top").removeClass('navbar-static-top').addClass('navbar-fixed-top');
    this.$body = $(document.body);
    this.$side = $('#side-menu');
  }

  helpers = {
    search() {
      return {
        name: 'top-search-in-app',
        className: 'navbar-form-custom',
        fields: [
          {
            template: 'text',
            field: 'top-search',
            props: {
              id: 'top-search',
              placeholder: 'Search for something...',
            },
          },
        ],
        submit: false,
      };
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {
    'click .js-top-logout'(e) {
      e.preventDefault();
      this.props.logout();
    },
    // Toggle left navigation
    'click .navbar-minimalize'(event) {
      event.preventDefault();

      // Toggle special class
      this.$body.toggleClass('mini-navbar');

      // Enable smoothly hide/show menu
      if (!this.$body.hasClass('mini-navbar') || this.$body.hasClass('body-small')) {
        // Hide menu in order to smoothly turn on when maximize menu
        this.$side.hide();
        // For smoothly turn on menu
        setTimeout(() => {
          this.$side.fadeIn(400);
        }, 200);
      } else if (this.$body.hasClass('fixed-sidebar')) {
        this.$side.hide();
        setTimeout(() => {
          this.$side.fadeIn(400);
        }, 200);
      } else {
        // Remove all inline style from jquery fadeIn function to reset menu state
        this.$side.removeAttr('style');
      }
    },

    // Toggle right sidebar
    'click .right-sidebar-toggle'() {
      $('#right-sidebar').toggleClass('sidebar-open');
    },
  };
}
