import { template, TemplateClass } from 'meteor/template-controller';
import { FlowRouter } from 'meteor/kadira:flow-router';
import SimpleSchema from 'simpl-schema';

import '../plugins/jquery.metisMenu';

// import template
import './navigation.html';

const ItemSchema = new SimpleSchema({
  show: { type: Boolean, defaultValue: true },
  name: { type: SimpleSchema.oneOf(String, Array) },
  'name.$': String,
  title: String,
  icon: { type: String, optional: true },
  badge: {
    type: new SimpleSchema({
      className: { type: String, defaultValue: 'primary' },
      value: { type: Number, defaultValue: 0 },
    }),
    optional: true,
  },
  subitems: { type: Array, defaultValue: [] },
  'subitems.$': {
    type: new SimpleSchema({
      name: { type: String },
      title: { type: String },
    }),
  },
});

@template('InspiniaNavigation')
class Navigation extends TemplateClass {
  props = new SimpleSchema({
    path: { type: String, defaultValue: '#' },
    name: { type: String, defaultValue: 'Para Dancing' },
    image: { type: String, defaultValue: '/packages/inspinia-theme/assets/images/profile.jpg' },
    items: { type: Array },
    'items.$': ItemSchema,
  });

  onRendered() {
    const sideMenu = this.$('#side-menu').metisMenu();
  };

  helpers = {
    active({ name }) {
      if (Array.isArray(name)) {
        return (name.some(n => n === FlowRouter.getRouteName()) && 'active') || '';
      }
      return (name === FlowRouter.getRouteName() && 'active') || '';
    },
    in({ name }) {
      return (name.some(n => n === FlowRouter.getRouteName()) && 'in') || '';
    },
    path({ name }) {
      // Когда пункт должен быть активным на нескольких роутах
      if (Array.isArray(name)) {
        // Первый роут это ссылка
        return FlowRouter.path(name[0]);
      }
      return FlowRouter.path(name);
    },
  };
}
