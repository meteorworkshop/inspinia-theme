import { template, TemplateClass } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './page.html';

@template('InspiniaPage')
class InspiniaPage extends TemplateClass {
  // Validate the properties passed to the template from parents
  props = new SimpleSchema({
    heading: { type: Object, blackbox: true },
  });
}
