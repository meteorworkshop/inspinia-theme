import { template, TemplateClass } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './page-heading.html';

const linkSchema = new SimpleSchema({
  name: { type: String, optional: true },
  label: String,
});

@template('InspiniaPageHeading')
class InspiniaPageHeading extends TemplateClass {
  // Validate the properties passed to the template from parents
  props = new SimpleSchema({
    title: String,
    home: { type: linkSchema, optional: true },
    links: { type: Array, defaultValue: [] },
    'links.$': { type: linkSchema, optional: true },
  });

  // Helpers work like before but <this> is always the template instance!
  helpers = {
    hasBreadcrumb() {
      return this.props.home || this.props.links.length;
    },
    url(name) {
      return FlowRouter.path(name);
    },
  };

  // Events work like before but <this> is always the template instance!
  events = {};

}
