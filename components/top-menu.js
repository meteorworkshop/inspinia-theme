import { template, TemplateClass } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './top-menu.html';

@template('InspiniaTopMenu')
class InspiniaTopMenu extends TemplateClass {
  // Validate the properties passed to the template from parents
  props = new SimpleSchema({
    name: { type: String, defaultValue: '' },
    className: { type: String, defaultValue: '' },
    logout: Function,
    signOut: { type: String, defaultValue: '' },
  });

  // Setup private reactive template state
  state = {};

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  }

  onRendered() {
  }

  onDestroyed() {
  }

  // Helpers work like before but <this> is always the template instance!
  helpers = {};

  // Events work like before but <this> is always the template instance!
  events = {
    'click .js-top-logout'(e) {
      e.preventDefault();
      this.props.logout();
    },
  };
}
