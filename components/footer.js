import { template, TemplateClass } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './footer.html';

@template('InspiniaFooter')
class InspiniaFooter extends TemplateClass {
  props = new SimpleSchema({
    className: { type: String, defaultValue: '' },
  });
}
