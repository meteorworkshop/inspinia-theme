import { checkNpmVersions } from 'meteor/tmeasday:check-npm-versions';

checkNpmVersions({
  'simpl-schema': '1.5.5',
  classnames: '2.2.6',
  jquery: '3.4.1',
}, 'blaze-form');

import 'popper.js';
import 'bootstrap';
import 'pace-progress';

//pace.start();

// Layouts
import './layouts/inspinia-layout-blank';
import './layouts/inspinia-layout-main';
import './layouts/inspinia-layout-top';
// Atoms
export * from './atoms';
// Components
import './components';
