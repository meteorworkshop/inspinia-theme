Package.describe({
  name: 'inspinia-theme',
  version: '0.1.1',
  summary: 'Meteor package for inspinia theme',
  git: 'https://bitbucket.org/meteorworkshop/inspinia-theme.git',
  documentation: 'README.md',
});

Npm.depends({
  'popper.js': '1.14.3',
  bootstrap: '4.3.1',
  'animate.css': '3.7.2',
  'pace-progress': '1.0.2',
});

Package.onUse((api) => {
  api.versionsFrom('1.8');
  api.use([
    'ecmascript',
    'fourseven:scss',
    'templating',
    'template-controller',
    'tmeasday:check-npm-versions',
    'kadira:flow-router',
    'font-awesome',
    'blaze-form',
  ]);

  api.addAssets([
    // fonts
    'assets/fonts/glyphicons-halflings-regular.eot',
    'assets/fonts/glyphicons-halflings-regular.svg',
    'assets/fonts/glyphicons-halflings-regular.ttf',
    'assets/fonts/glyphicons-halflings-regular.woff',
    'assets/fonts/glyphicons-halflings-regular.woff2',
    'assets/fonts/open-sans-light.ttf',
    'assets/fonts/open-sans-regular.ttf',
    'assets/fonts/open-sans-semi-bold.ttf',
    'assets/fonts/open-sans-bold.ttf',
    'assets/fonts/roboto-light.ttf',
    'assets/fonts/roboto-regular.ttf',
    'assets/fonts/roboto-medium.ttf',
    'assets/fonts/roboto-bold.ttf',
    // patterns
    'assets/patterns/header-profile.png',
    'assets/patterns/header-profile-skin-1.png',
    'assets/patterns/header-profile-skin-2.png',
    'assets/patterns/header-profile-skin-3.png',
    'assets/patterns/header-profile-skin-md.png',
    'assets/patterns/shattered.png',
    // images
    'assets/images/profile.jpg',
  ], 'client');

  api.addFiles([
    'stylesheets/inspinia/_variables.scss',
  ], 'client', { isImport: true });

  api.addFiles([
    'stylesheets/bootstrap/bootstrap.scss',
    'stylesheets/inspinia/style.scss',
    'stylesheets/animate.css',
  ], 'client');

  api.mainModule('inspinia-theme.js', 'client');
});
