import { template, TemplateClass } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

import './inspinia-layout-blank.html';

@template('InspiniaLayoutBlank')
class LayoutBlank extends TemplateClass {
  props = new SimpleSchema({
    content: { type: Function },
  });

  onRendered() {
    $(document.body).addClass('gray-bg');
  }

  onDestroyed() {
    $(document.body).removeClass('gray-bg');
  }
}
