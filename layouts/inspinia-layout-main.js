import { template, TemplateClass } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import './inspinia-layout-main.html';

@template('InspiniaLayoutMain')
class LayoutMain extends TemplateClass {
  // Validate the properties passed to the template from parents
  props = new SimpleSchema({
    content: { type: Function },
    menu: { type: Function },
    top: { type: Function },
  });

  onRendered() {
    // todo переписать генерацию начальных параметров
    // Minimalize menu when screen is less than 768px
    $(window).bind('resize load', function () {
      if ($(this).width() < 769) {
        $('body').addClass('body-small');
      } else {
        $('body').removeClass('body-small');
      }
    });

    // Fix height of layout when resize, scroll and load
    $(window).bind('load resize scroll', function () {
      if (!$(document.body).hasClass('body-small')) {
        const navbarHeight = $('nav.navbar-default').height();
        const wrapper = this.$('#page-wrapper');
        const wrapperHeight = wrapper.height();

        if (navbarHeight > wrapperHeight) {
          wrapper.css('min-height', `${navbarHeight}px`);
        }

        if (navbarHeight < wrapperHeight) {
          wrapper.css('min-height', `${$(window).height()}px`);
        }

        if ($('body').hasClass('fixed-nav')) {
          if (navbarHeight > wrapperHeight) {
            wrapper.css('min-height', `${navbarHeight - 60}px`);
          } else {
            wrapper.css('min-height', `${$(window).height() - 60}px`);
          }
        }
      }
    });
  }
}
