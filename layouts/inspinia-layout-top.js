import { template, TemplateClass } from 'meteor/template-controller';
import SimpleSchema from 'simpl-schema';

// import components
import './inspinia-layout-top.html';

@template('InspiniaLayoutTop')
class LayoutTop extends TemplateClass {
  // Validate the properties passed to the template from parents
  props = new SimpleSchema({
    content: { type: Function },
    menu: { type: Function },
  });

  onRendered() {
    $(document.body).addClass('top-navigation gray-bg');
  }

  onDestroyed() {
    $(document.body).removeClass('top-navigation gray-bg');
  }
}
