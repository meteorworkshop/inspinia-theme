import SimpleSchema from 'simpl-schema';
import Component from '../component';
// import template
import './spinner.html';

Template.registerHelper('isLoading', loading => (loading && 'sk-loading') || '');

Template.registerHelper('isNotLoading', loading => (!loading && 'sk-loading') || '');

const Spinner = new Component('Spinner', {
  props: new SimpleSchema({
    type: {
      type: String,
      label: 'Тип спиннера',
      allowedValues: ['wandering-cubes', 'fading-circle', 'double-bounce'],
      defaultValue: 'wandering-cubes',
    },
  }),
  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    isType(type) {
      return type === this.props.type;
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {},
});

export default Spinner;
