import SimpleSchema from 'simpl-schema';
import c from 'classnames';
import Component from '../component';
// import template
import './button.html';


const Button = new Component('Button', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    active: { type: Boolean, optional: true },
    block: { type: Boolean, optional: true },
    color: {
      type: String,
      defaultValue: 'primary',
      allowedValues: [...Component.colors, 'link'],
    },
    disabled: { type: Boolean, optional: true },
    outline: { type: Boolean, optional: true },
    tag: { type: String, defaultValue: 'button', allowedValues: ['link', 'button', 'input'] },
    type: {
      type: String,
      optional: true,
      autoValue() {
        if (this.field('tag').value !== 'link' && !this.field('href').value && !this.isSet) {
          return 'button';
        }
      },
      allowedValues: ['submit', 'button', 'reset'],
    },
    onClick: { type: Function, optional: true },
    size: { type: String, optional: true, allowedValues: ['xs', 'sm', 'lg'] },
    href: {
      type: String,
      optional: true,
      autoValue() {
        if (this.field('tag').value === 'link' && !this.isSet) {
          return '#';
        }
      },
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    isButton() {
      return this.props.tag === 'button' && !this.props.href;
    },
    isLink() {
      return this.props.tag === 'link' || this.props.href;
    },
    isInput() {
      return this.props.tag === 'input' && !this.props.href;
    },
    attributes() {
      const {
        outline, tag, color, size, block, href, active, disabled, type, className, attributes,
      } = this.props;
      const classes = c({
        [this.cp]: true,
        [`${this.cp}${outline ? '-outline' : ''}-${color}`]: true,
        [`${this.cp}-${size}`]: !!size,
        [`${this.cp}-block`]: block,
        active,
        disabled,
        [`${className}`]: className,
      });
      if (tag === 'input' && !href) {
        attributes.value = this.props.content;
      }
      return {
        type,
        href,
        class: classes,
        'aria-pressed': active,
        'aria-disabled': disabled,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {
    'click .btn'(e) {
      if (this.props.onClick) {
        e.preventDefault();
        this.props.onClick();
      }
    },
  },

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'btn',
  },
});

export default Button;
