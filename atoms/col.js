import c from 'classnames';
import SimpleSchema from 'simpl-schema';
import Component from '../component';
// import template
import './col.html';

const vertical = {
  type: String,
  optional: true,
  allowedValues: ['start', 'center', 'end', 'baseline', 'stretch'],
};

const prefixes = ['', '-sm', '-md', '-lg', '-xl'];

const size = {
  type: SimpleSchema.oneOf(Number, Boolean, String),
  optional: true,
  allowedValues: ['auto'],
  min: 1,
  max: 12,
};

const order = {
  type: SimpleSchema.oneOf(Number, String),
  optional: true,
  allowedValues: ['first', 'last'],
  min: 0,
  max: 12,
};

const offset = {
  type: Number,
  optional: true,
  min: 0,
  max: 12,
};

const ColumnObject = new SimpleSchema({
  size,
  order,
  offset,
  vertical,
});

const Column = {
  type: SimpleSchema.oneOf(Number, Boolean, ColumnObject, String),
  optional: true,
  allowedValues: ['auto'],
  min: 1,
  max: 12,
};

const Col = new Component('Col', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    size: { ...size, defaultValue: true },
    order,
    offset,
    vertical,
    xs: Column,
    sm: Column,
    md: Column,
    lg: Column,
    xl: Column,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const {
        size: s, order: or, offset: of, vertical: v, xs, sm, md, lg, xl, className, attributes,
      } = this.props;

      const classes = c(
        {
          [this.getColumnSize(s, '')]: s,
          // order и offset можут быть 0
          [`order-${or}`]: or || or === 0,
          [`offset-${of}`]: of || of === 0,
          [this.getVerticalClass(v, '')]: v,
        },
        [xs, sm, md, lg, xl].map(this.getColumnClass.bind(this)),
        className,
      );
      return {
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'col',
    /**
     * Формирование класса для колонки
     * @param index индекс для классов
     * @param value {true|'auto'|number}
     * @returns {string|object}
     */
    getColumnClass(value, index) {
      if (!value) {
        return '';
      }
      const prefix = prefixes[index];
      if (typeof value !== 'object') {
        return this.getColumnSize(value, prefix);
      }
      const {
        size: s, order: or, offset: of, vertical: v,
      } = value;
      return {
        [this.getColumnSize(s, prefix)]: s,
        [`order${prefix}-${or}`]: or || or === 0,
        [`offset${prefix}-${of}`]: of || of === 0,
        [this.getVerticalClass(v, prefix)]: v,
      };
    },
    getColumnSize(value, prefix) {
      let sizeClass = `-${value}`;
      // col col-sm col-lg
      if (value === true || !value) {
        sizeClass = '';
      }

      return `col${prefix}${sizeClass}`;
    },
    getVerticalClass(align, prefix) {
      return `align-self${prefix}-${align}`;
    },
  },
});

export { Col as default, vertical, prefixes };
