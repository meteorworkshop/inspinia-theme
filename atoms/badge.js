import c from 'classnames';
import SimpleSchema from 'simpl-schema';
import Component from '../component';
// import template
import './badge.html';

const Badge = new Component('Badge', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    color: {
      type: String,
      defaultValue: 'primary',
      allowedValues: Component.colors,
    },
    href: {
      type: String,
      optional: true,
    },
    pill: {
      type: Boolean,
      defaultValue: false,
    },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    isA() {
      return this.props.href;
    },
    attributes() {
      const {
        color, pill, className, attributes,
      } = this.props;

      const classes = c(
        {
          [this.cp]: true,
          [`${this.cp}-${color}`]: color,
          [`${this.cp}-pill`]: pill,
        },
        className,
      );
      return {
        class: classes,
        ...attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    // class parent
    cp: 'badge',
  },
});

export { Badge as default };
