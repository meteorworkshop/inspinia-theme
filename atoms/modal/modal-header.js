import SimpleSchema from 'simpl-schema';
import Component from '../../component';
// import template
import './modal-header.html';

const ModalHeader = new Component('ModalHeader', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    dismiss: { type: Boolean, optional: true },
    title: { type: String, optional: true },
    id: { type: String, optional: true },
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      return {
        class: `${this.cp}-header ${this.props.className}`,
        ...this.props.attributes,
      };
    },
    title() {
      return {
        class: `${this.cp}-title`,
        id: this.props.id,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'modal',
  },
});

export default ModalHeader;

