import { Random } from 'meteor/random';
import SimpleSchema from 'simpl-schema';
import c from 'classnames';

import Component from '../../component';
// import template
import './modal.html';

const Modal = new Component('Modal', {
  // Validate the properties passed to the template from parents
  props: new SimpleSchema({
    fade: { type: Boolean, defaultValue: true },
    open: { type: Boolean, defaultValue: false },
    centered: { type: Boolean, optional: true },
    dismiss: { type: Boolean, defaultValue: true },
    title: { type: String, optional: true },
    size: { type: String, optional: true, allowedValues: ['lg', 'sm'] },
    handleModal: { type: Function, optional: true },
    onClose: Function,
  }),

  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
    this.id = Random.id();
  },
  onRendered() {
    const { onClose, handleModal } = this.props;
    this.modal = this.$('.modal');
    this.modal.on('hidden.bs.modal', () => {
      if (onClose) {
        onClose();
      }
    });
    if (handleModal) {
      handleModal(this.modal);
    }
    this.autorun(() => {
      if (this.props.open) {
        this.modal.modal('show');
      } else {
        this.modal.modal('hide');
      }
    });
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      const { fade, className, attributes } = this.props;
      const classes = c({
        [this.cp]: true,
        inmodal: true,
        fade,
        [`${className}`]: className,
      });
      return {
        class: classes,
        tabindex: '-1',
        role: 'dialog',
        'aria-labelledby': this.id,
        'aria-hidden': true,
        ...attributes,
      };
    },
    dialog() {
      const { centered, size } = this.props;
      return {
        class: c({
          [`${this.cp}-dialog`]: true,
          [`${this.cp}-dialog-centered`]: centered,
          [`${this.cp}-${size}`]: size,
        }),
        role: 'document',
      };
    },
    content() {
      return {
        class: `${this.cp}-content`,
      };
    },
    needHeader() {
      return this.props.title || this.props.dismiss;
    },
    header() {
      const { title, dismiss } = this.props;
      return { title, dismiss, id: this.id };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'modal',
    modal: null,
  },
});

export default Modal;
