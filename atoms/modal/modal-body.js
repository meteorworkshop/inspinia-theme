import Component from '../../component';
// import template
import './modal-body.html';

const ModalBody = new Component('ModalBody', {
  // Setup private reactive template state
  state: {},

  // Lifecycle callbacks work exactly like with standard Blaze
  onCreated() {
  },
  onRendered() {
  },
  onDestroyed() {
  },

  // Helpers work like before but <this> is always the template instance!
  helpers: {
    attributes() {
      return {
        class: `${this.cp}-body ${this.props.className}`,
        ...this.props.attributes,
      };
    },
  },

  // Events work like before but <this> is always the template instance!
  events: {},

  // These are added to the template instance but not exposed to the html
  private: {
    cp: 'modal',
  },
});

export default ModalBody;

